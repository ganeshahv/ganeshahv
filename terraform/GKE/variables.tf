variable "project_id" {
  default     = "vesio-dev-cz"
  description = "project id"
}

variable "region" {
  default     = "us-east1-c"
  description = "region"
}

variable "location" {
  default     = "us-east1"
  description = "location"
}

variable "cluster_name" {
  default     = "buytime-gke-cluster"
  description = "cluster name"
}

variable "gke_num_nodes" {
  default     = 1
  description = "number of nodes per zone"
}

variable "machine_type" {
  default     = "e2-standard-4"
  description = "node machine type"
}

variable "kubernetes_network_ipv4_cidr" {
  type        = string
  default     = "101.0.96.0/22"
  description = "IP CIDR block for the subnetwork. This must be at least /22 and cannot overlap with any other IP CIDR ranges."
}

variable "kubernetes_pods_ipv4_cidr" {
  type        = string
  default     = "102.0.92.0/22"
  description = "IP CIDR block for pods. This must be at least /22 and cannot overlap with any other IP CIDR ranges."
}

variable "kubernetes_services_ipv4_cidr" {
  type        = string
  default     = "103.0.88.0/22"
  description = "IP CIDR block for services. This must be at least /22 and cannot overlap with any other IP CIDR ranges."
}
