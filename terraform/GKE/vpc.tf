resource "google_compute_subnetwork" "subnet" {
  name          = "${var.project_id}-cluster-subnet"
  region        = var.location
  network       = google_compute_network.vpc.id
  ip_cidr_range = var.kubernetes_network_ipv4_cidr

  private_ip_google_access = true

  secondary_ip_range {
    range_name    = "pods-cidr"
    ip_cidr_range = var.kubernetes_pods_ipv4_cidr
  }

  secondary_ip_range {
    range_name    = "svcs-cidr"
    ip_cidr_range = var.kubernetes_services_ipv4_cidr
  }
}


resource "google_compute_network" "vpc" {
  name                    = "${var.project_id}-cluster-vpc"
  auto_create_subnetworks = false
}
